const iconOpenNavMobile = document.querySelector('.header-switch-nav-mobile .icon-open');
const iconCloseNavMobile = document.querySelector('.header-switch-nav-mobile .icon-close');

const navbarMobile = document.querySelector('.header-nav-mobile');
const wrapSlider = document.querySelector('.wrap-slider');
const backToTop = document.querySelector('.wrap-back-to-top');

iconOpenNavMobile.addEventListener('click', () => {
  navbarMobile.classList.add('header-nav-mobile__open');
  iconCloseNavMobile.classList.remove('hidden');
  iconOpenNavMobile.classList.add('hidden');
  wrapSlider.setAttribute("style", "z-index: 0;");
  navbarMobile.setAttribute("style", "z-index: 30;");
});

iconCloseNavMobile.addEventListener('click', () => {
  navbarMobile.classList.remove('header-nav-mobile__open');
  iconOpenNavMobile.classList.remove('hidden');
  iconCloseNavMobile.classList.add('hidden');
  wrapSlider.setAttribute("style", "z-index: 30;");
  navbarMobile.setAttribute("style", "z-index: 0;");
});


// slider -banner
const swiper = new Swiper(".mySwiperSlider", {
  slidesPerView: 1,
  spaceBetween: 10,
  loop: true,
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
  effect: 'slide',
  pagination: {
    el: ".swiper-pagination",
    clickable: true,
  },
  breakpoints: {
    640: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 1,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 1,
      spaceBetween: 30,
    },
  },
});

// slider-clients
const swiperClients = new Swiper(".main-content-header__swipper-clients", {
  slidesPerView: 1,
  spaceBetween: 10,
  loop: true,
  autoplay: {
    delay: 1000,
    disableOnInteraction: false,
  },

  breakpoints: {
    640: {
      slidesPerView: 2,
      spaceBetween: 6,
    },
    768: {
      slidesPerView: 5,
      spaceBetween: 20,
    },
    1024: {
      slidesPerView: 7,
      spaceBetween: 30,
    },
  },
});


// back top top
window.addEventListener('scroll', () => {
  if (window.scrollY >= 600) {
    backToTop.classList.remove('hidden');
  } else {
    backToTop.classList.add('hidden');
  }
});

if (backToTop) {
  backToTop.addEventListener('click', () => {
    window.scrollTo(0, 0);
  })
};